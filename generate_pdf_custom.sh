#!/bin/sh
set -ex

# I. Merge all md
mdArray=(
  "index.md"\
  "trust_anchors.md"\
  "participant.md"\
  "service.md"\
  "examples.md"
)

for file in ${mdArray[@]}; do
  echo $file
  cat docs/$file >> docs/trust_framework.md
done

# II. Enumerate Headings
markdown-enum docs/trust_framework.md 1 docs/trust_framework.md

# # III. Create Table of Content
toc=$(bash create_TOC.sh docs/trust_framework.md)
echo "$toc" >> temp.md
echo " " >> temp.md
cat docs/trust_framework.md >> temp.md
rm docs/trust_framework.md
mv temp.md docs/trust_framework.md

# IV. Convert mermaid graph to png
mkdir output
bash convert_mermaid_graphs.sh docs/trust_framework.md

# V. Move trust_framework and png to docs
mkdir temp
mv docs/trust_framework.md temp/
mv output/*.png temp/


